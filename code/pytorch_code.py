#!/usr/bin/env python

import os
import torch
import argparse
import numpy as np
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F

from torch.autograd import Variable

from helpers.grapher import Grapher
from helpers.utils import softmax_accuracy

parser = argparse.ArgumentParser(description='My Simple Neural Network')


# Task parameters
parser.add_argument('--task', type=str, default='mnist',
                    help='dataset to work with (default: mnist)')
parser.add_argument('--data-dir', type=str, default='./.datasets', metavar='DD',
                    help='directory which contains input data')
parser.add_argument('--lr', type=float, default=1e-3,
                    help='learning rate (default: 1e-3)')
parser.add_argument('--optimizer', type=str, default='adam',
                    help='optimizer (default: adam)')

# Model parameters
parser.add_argument('--uid', type=str, default='dense',
                    help='custom UID for this model')
parser.add_argument('--layers', nargs='+', default="32",
                    help="layer sizing (default: single layer [32])")
parser.add_argument('--batch-size', type=int, default=256,
                    help='batch size (default: 256)')
parser.add_argument('--epochs', type=int, default=10,
                    help='minimum number of epochs to train (default: 10)')
parser.add_argument('--activation', type=str, default="elu",
                    help='activation function (default: elu)')
parser.add_argument('--normalizer', type=str, default=None,
                    help='normalizer function (eg: batch_norm, layer_norm) (default: None)')

# Device parameters
parser.add_argument('--seed', type=int, default=None,
                    help='seed for numpy and pytorch (default: None)')
parser.add_argument('--ngpu', type=int, default=1,
                    help='number of gpus available (default: 1)')
parser.add_argument('--no-cuda', action='store_true', default=False,
                    help='disables CUDA training')
args = parser.parse_args()
args.cuda = not args.no_cuda and torch.cuda.is_available()


def create_dir(path):
    ''' helper to create directories '''
    if not os.path.exists(path):
        os.makedirs(path) # mkdirs is recursive


class Dataset(torch.utils.data.Dataset):
    def __init__(self, h5py_loader, batch_size):
        ''' a dataset simply needs to implement getitem and len '''
        self.loader = h5py_loader
        self.num_samples = len(self.loader)
        self.batch_size = batch_size
        self.current = 0

    def __getitem__(self, index):
        if index > self.num_samples:
            raise StopIteration()

        # simply direcly index
        return self.loader['data'][index], \
            self.loader['labels'][index]

    def __len__(self):
        return self.num_samples


class DataLoader(object):
    def __init__(self, args):
        data_map = DataLoader.load_data(args)
        val_dataset = Dataset(data_map['val'], args.batch_size)
        train_dataset = Dataset(data_map['train'], args.batch_size)
        test_dataset = Dataset(data_map['test'], args.batch_size)
        self.input_size = 784 # eg: this will be fixed based on the dataset
        self.output_size = 10 # eg: mnist has 10 classes, ok to be fixed

        # build the actual dataloaders
        kwargs = {'num_workers': 2, 'pin_memory': True} if args.cuda else {}
        self.train = torch.utils.data.DataLoader(train_dataset,
                                                 batch_size=args.batch_size,
                                                 shuffle=True,
                                                 **kwargs)
        self.val = torch.utils.data.DataLoader(val_dataset,
                                                 batch_size=args.batch_size,
                                                 shuffle=True,
                                                 **kwargs)
        self.test = torch.utils.data.DataLoader(test_dataset,
                                                 batch_size=args.batch_size,
                                                 shuffle=True,
                                                 **kwargs)

    @staticmethod
    def load_data(args):
        ''' eg: read h5py structure;
            NOTE: this doesn't load the entire file, just the header'''
        train_data, train_labels \
            = load_your_data_here(args.data_dir, args.batch_size, split='train')
        val_data, val_labels \
            = load_your_data_here(args.data_dir, args.batch_size, split='val')
        test_data, test_labels \
            = load_your_data_here(args.data_dir, args.batch_size, split='test')
        return {
            # 'output_size': output_size,  # should be extracted here
            # 'input_size': input_size,    # should be extracted here
            'train' : {
                'data': train_data,
                'labels': train_labels
            },
            'val': {
                'data': val_data,
                'labels': val_labels
            },
            'test': {
                'data': test_data,
                'labels': test_labels
            }
        }


class EarlyStopping(object):
    def __init__(self, model, max_steps=10, save_best=True):
        self.max_steps = max_steps
        self.model = model
        self.save_best = save_best

        self.loss = 0.0
        self.iteration = 0
        self.stopping_step = 0
        self.best_loss = np.inf

    def restore(self):
        self.model.restore()

    def __call__(self, loss):
        if (loss < self.best_loss):
            self.stopping_step = 0
            self.best_loss = loss
            if self.save_best:
                self.model.save(overwrite=True)
        else:
            self.stopping_step += 1

        is_early_stop = False
        if self.stopping_step >= self.max_steps:
            print("Early stopping is triggered;  loss:{} | iter: {}".format(loss, self.iteration))
            is_early_stop = True

        self.iteration += 1
        return is_early_stop


class NNModel(nn.Module):
    def __init__(self, input_size, output_size, args):
        ''' the main neural network model class '''
        super(NNModel, self).__init__()
        self.config = vars(args)
        self.input_size = input_size
        self.output_size = output_size

        # the layer sizes parsed from args
        layer_sizes = list(args.layers) if not isinstance(args.layers, list) else args.layers
        assert len(layer_sizes) >= 1, "Need at least 1 layer"

        # build the graph and get the logit outputs
        self.model = self._build_model(layer_sizes)

    @staticmethod
    def get_normalizer(normalizer_str):
        '''
        Helper to get normalizer function and params
        '''
        normalizer_map = {
            'batch_norm': nn.BatchNorm1d,
            'dropout': nn.Dropout,
        }
        return normalizer_map[normalizer_str]

    def _build_model(self, layer_sizes):
        ''' helper to create our model
            Conv/FC --> BN --> Activ --> Dropout'''
        #create the first layer as it goes from input --> hidden_0
        layer_list = [nn.Linear(self.input_size, layer_sizes[0])]

        # create other layers (up till second to last)
        for input_size, output_size in zip(layer_sizes[0:-1], layer_list[1:-1]):
            layer_list.append(nn.Linear(input_size, output_size))
            if self.config['normalizer_str'] is not None:
                layer_list.append(NNModel.get_normalizer(self.config['normalizer_str']))

        # the last is unactivated because if you use sigm-xentropy
        # there is a more stable version provided by tf / pytorch, etc
        layer_list.append(nn.Linear(layer_list[-1], self.output_size))
        model = nn.ModuleList(layer_list)

        if self.config['ngpu'] > 1:
            model = nn.DataParallel(model)

        if self.config['cuda']:
            model.cuda()

    def loss(self, predictions, true_values, size_average=False):
        '''simple mean squared error loss '''
        return F.mse_loss(predictions, true_values, size_average=size_average)

    def forward(self, x):
        ''' modulelist requires forwarding through each layer '''
        for layer in self.model:
            x = layer(x)

        return x


def build_optimizer(model, args):
    optim_map = {
        "rmsprop": optim.RMSprop,
        "adam": optim.Adam,
        "adadelta": optim.Adadelta,
        "sgd": optim.SGD,
        "lbfgs": optim.LBFGS
    }

    return optim_map[args.optimizer.lower().strip()](
        model.parameters(), lr=args.lr
    )


def train(epoch, model, optimizer, data_loader, args):
    ''' helper to train the model with the provided dataset and optimizer '''
    model.train()
    for batch_idx, (data, target) in enumerate(data_loader.train_loader):
        if args.cuda:
            data, target = data.cuda(), target.cuda()

        data, target = Variable(data), Variable(target.squeeze())

        # zero grads of optimizer
        optimizer.zero_grad()

        # project to the output dimension
        output, _ = model(data)
        loss = model.loss_function(output, target)
        correct = softmax_accuracy(output, target)

        # compute backward pass and optimize
        loss.backward()
        optimizer.step()

        # log every nth interval
        if batch_idx % args.log_interval == 0:
            num_samples = len(data_loader.train_loader.dataset)
            print('Train Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {:.6f}\tAccuracy: {:.4f}'.format(
                epoch, batch_idx * len(data), num_samples,
                100. * batch_idx * len(data) / num_samples,
                loss.data[0], correct))


def test_or_val(epoch, model, data_loader, args):
    ''' helper to test and produce avg loss given model and dataset '''
    model.eval()
    loss, correct = [], []
    for data, target in data_loader.test_loader:
        if args.cuda:             # move to GPU
            data, target = data.cuda(), target.cuda()

        with torch.no_grad():
            # variables are needed for computing gradients
            data, target = Variable(data), Variable(target.squeeze())

            # forward pass
            output = model(data)

            # compute model loss and evaluate accuracy
            # place into a list so that we can tabulate mean
            loss = model.loss_function(output, target)
            correct = softmax_accuracy(output, target)
            loss.append(loss.detach().cpu().data[0])
            correct.append(correct)

    # no need to do a running mean, just place in list
    loss, acc = np.mean(loss), np.mean(correct)
    print('\nTest Epoch: {}\tAverage loss: {:.4f}\tAverage Accuracy: {:.4f}\n'.format(
        epoch, loss, acc
    ))
    return loss, acc


def run(args):
    # get the dataloader
    loader = DataLoader(args)

    # build the model and early stopper
    model = NNModel(loader.input_size, loader.output_size, args)    # build our model
    early = EarlyStopping(model)   # get the early stopper

    # a visdom grapher object to enable matplotlib api
    grapher = Grapher(env=model.get_name(),
                      server=args.visdom_url,
                      port=args.visdom_port)

    for epoch in range(args.epochs + 1):
        train(epoch, model, loader.train, model.train_summary_writer, epoch)
        val_loss, _ = test_or_val(epoch, model, loader.val, args)

        # handle early stopping
        if early(val_loss):
            early.restore()
            test_or_val(epoch, model, loader.test, args)
            break

        # evaluate test metrics
        test_loss, acc = test_or_val(epoch, model, loader.test, args)


if __name__ == "__main__":
    # seed our RNGs
    np.random.seed(args.seed)
    torch.manual_seed_all(args.seed)

    # build the dirs for storage of logs and models
    create_dir(args.output_dir)
    create_dir(os.path.join(args.output_dir, "logs"))
    create_dir(os.path.join(args.output_dir, "models"))

    # main entrypoint
    run(args)
