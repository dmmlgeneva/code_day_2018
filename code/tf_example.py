#!/usr/bin/env python

import os
import time
import argparse
import scipy.io
import numpy as np
import tensorflow as tf
import tensorflow.contrib.slim as slim

parser = argparse.ArgumentParser(description='My Simple Neural Network')

# General parameters
parser.add_argument('--seed', type=int, default=1,
                    help="seed for rng (default: 1)")
parser.add_argument("--device", type=str, default="/gpu:0", help="Compute device.")
parser.add_argument('--allow-soft-placement', action='store_true',
                    help='Place on GPU + CPU')
parser.add_argument("--device-percentage", type=float, default=0.3, help="Amount of memory to use on device. (default: 0.3)")

# Task parameters
parser.add_argument('--data-dir', type=str, default='./.datasets', metavar='DD',
                    help='directory which contains input data')
parser.add_argument('--output-dir', type=str, default='./output',
                    help='directory for output of logs for tensorboard')
parser.add_argument('--input-size', type=int, default=10,
                    help='output size (default: 1000)') # better to use loader to extract this
parser.add_argument('--output-size', type=int, default=10,
                    help='output size (default: 10)')   # better to use loader to extract this
parser.add_argument('--lr', type=float, default=1e-3,
                    help='learning rate (default: 1e-3)')

# Model parameters
parser.add_argument('--uid', type=str, default='dense',
                    help='custom UID for this model')
parser.add_argument('--layers', nargs='+', default="32",
                    help="layer sizing (default: single layer [32])")
parser.add_argument('--batch-size', type=int, default=256,
                    help='batch size (default: 256)')
parser.add_argument('--epochs', type=int, default=10,
                    help='minimum number of epochs to train (default: 10)')
parser.add_argument('--activation', type=str, default="elu",
                    help='activation function (default: elu)')
parser.add_argument('--normalizer', type=str, default=None,
                    help='normalizer function (eg: batch_norm, layer_norm) (default: None)')

# actually parse the arguments
args = parser.parse_args()

def create_dir(path):
    ''' helper to create directories '''
    if not os.path.exists(path):
        os.makedirs(path) # mkdirs is recursive


class TFRecordDataIterator():
    def __init__(self, sess, dataset, batch_size):
        ''' accepts a tf.data.Dataset and builds an iterator '''
        dataset = dataset.repeat() # repeat forever
        dataset = dataset.batch(batch_size)
        self.iterator = dataset.make_initializeable_iterator()
        sess.run(self.iterator) # see docs for more

    def reset(self, sess):
        ''' re-initialize the iterator '''
        sess.run(self.iterator)

    def __next__(self):
        # Python 3 compatibility
        return self.next()

    def next(self):
        return self.iterator.get_next()


class DataIterator(object):
    def __init__(self, generic_loader, batch_size):
        ''' read h5py/tfrecord from index [begin, end]
            This is very-prototype like; needs WORK
        '''
        self.loader = generic_loader
        self.num_samples = len(generic_loader)
        self.batch_size = batch_size
        self.current = 0

    def __next__(self):
        # Python 3 compatibility
        return self.next()

    def next(self):
        if self.current < self.num_samples:
            samples, labels = [
                self.loader['data'][self.current:self.current+self.batch_size],
                self.loader['labels'][self.current:self.current+self.batch_size]
            ]
            self.current += self.batch_size
            return [samples, labels]
        else:
            raise StopIteration()


class DataLoader(object):
    def __init__(self, args):
        data_map = DataLoader.load_data(args)
        self.val = DataIterator(data_map['val'], args.batch_size)
        self.train = DataIterator(data_map['train'], args.batch_size)
        self.test = DataIterator(data_map['test'], args.batch_size)

    @staticmethod
    def load_data(args):
        ''' read TFRecord data and build an iterator.
            NOTE: this does NOT read the entire dataset '''
        train_data, train_labels \
            = load_your_data_here(args.data_dir, args.batch_size, split='train')
        val_data, val_labels \
            = load_your_data_here(args.data_dir, args.batch_size, split='val')
        test_data, test_labels \
            = load_your_data_here(args.data_dir, args.batch_size, split='test')
        return {
            # 'output_size': output_size,  # should be extracted here
            # 'input_size': input_size,    # should be extracted here
            'train' : {
                'data': train_data,
                'labels': train_labels
            },
            'val': {
                'data': val_data,
                'labels': val_labels
            },
            'test': {
                'data': test_data,
                'labels': test_labels
            }
        }


class EarlyStopping(object):
    def __init__(self, model, max_steps=10, save_best=True):
        self.max_steps = max_steps
        self.model = model
        self.save_best = save_best

        self.loss = 0.0
        self.iteration = 0
        self.stopping_step = 0
        self.best_loss = np.inf

    def restore(self, sess):
        self.model.restore(sess)

    def __call__(self, loss):
        if (loss < self.best_loss):
            self.stopping_step = 0
            self.best_loss = loss
            if self.save_best:
                self.model.save(sess, overwrite=True)
        else:
            self.stopping_step += 1

        is_early_stop = False
        if self.stopping_step >= self.max_steps:
            print("Early stopping is triggered;  loss:{} | iter: {}".format(loss, self.iteration))
            is_early_stop = True

        self.iteration += 1
        return is_early_stop


class NNModel(object):
    def __init__(self, sess, args):
        ''' the main neural network model class '''
        self.config = vars(args)
        self.x = tf.placeholder(tf.float32, [None, args.input_size], name="input")
        self.y = tf.placeholder(tf.float32, [None, args.output_size], name="output")
        self.is_training = tf.placeholder(tf.bool)

        # the layer sizes parsed from args
        layer_sizes = list(args.layers) if not isinstance(args.layers, list) else args.layers
        assert len(layer_sizes) >= 1, "Need at least 1 layer"

        # build the graph and get the logit outputs
        self.model = self._build_model(layer_sizes)
        self.pred_logits = self.model(self.x)
        self.pred = tf.nn.sigmoid(self.pred_logits)

        # build the loss
        self.loss = tf.nn.l2_loss(self.pred - self.y, name="loss")

        # build the optimizer
        self.optim = tf.train.AdamOptimizer(
            learning_rate=self.config['lr']
        ).minimize(self.loss)

        # build all the summaries and the writers
        self.summaries = tf.summary.merge(self.get_summaries())
        self.train_summary_writer = tf.summary.FileWriter("%s/logs/train" % self.config['output_dir'],
                                                          sess.graph,
                                                          flush_secs=60)
        self.val_summary_writer = tf.summary.FileWriter("%s/logs/val" % self.config['output_dir'],
                                                        sess.graph,
                                                        flush_secs=60)
        self.test_summary_writer = tf.summary.FileWriter("%s/logs/test" % self.config['output_dir'],
                                                         sess.graph,
                                                         flush_secs=60)

        # build a saver object
        self.saver = tf.train.Saver(tf.global_variables() +
                                    tf.local_variables())

    def get_summaries(self):
        return [
            tf.summary.scalar("loss", self.loss)
            # add other summaries here
        ]

    @staticmethod
    def get_normalizer(is_training, normalizer_str):
        '''
        Helper to get normalizer function and params
        '''
        batch_norm_params = {'is_training': is_training,
                             'decay': 0.999, 'center': True,
                             'scale': True, 'updates_collections': None}
        layer_norm_params = {'center': True, 'scale': True}
        normalizer_fn, normalizer_params = None, None

        if 'layer_norm' in normalizer_str:
            print('using layer norm')
            normalizer_fn = slim.layer_norm
            normalizer_params = layer_norm_params
        elif 'batch_norm' in normalizer_str:
            print('using batch norm')
            normalizer_fn = slim.batch_norm
            normalizer_params = batch_norm_params
        else:
            print('not using any layer normalization scheme')
            normalizer_fn = None
            normalizer_params = None

        return [normalizer_fn, normalizer_params]

    def get_name(self):
        '''return a unique name for this model,
           add more here to make more unique '''
        return "{}_activ{}_batch{}_epochs{}_layers{}".format(
            self.config['uid'],
            str(self.config['activation']),
            str(self.config['batch_size']),
            str(self.config['epochs']),
            "_".join(self.config['layers'])
        )

    def save(self, sess, overwrite=False):
        model_filename = "{}/models/{}.cpkt".format(
            self.config['output_dir'], self.get_name()
        )
        if not os.path.isfile(model_filename) or overwrite:
            print('saving model to %s...' % model_filename)
            self.saver.save(sess, model_filename)

    def restore(self, sess):
        model_filename = "{}/models/{}.cpkt".format(
            self.config['output_dir'], self.get_name()
        )
        if os.path.isfile(model_filename):
            print('restoring model from %s...' % model_filename)
            self.saver.restore(sess, model_filename)

    @staticmethod
    def get_activation_fn(activ):
        ''' helper to get the desired activation from a string '''
        activ_map = {
            'elu': tf.nn.elu,
            'relu': tf.nn.relu,
            'sigmoid': tf.nn.sigmoid,
            'softmax': tf.nn.softmax,
            'tanh': tf.nn.tanh
        }
        return activ_map[activ]

    def _build_model(self, layer_sizes):
        ''' build the computation graph'''
        with tf.variable_scope("model"):
            # get all the initializers, the activation and the normalizations
            winit = tf.contrib.layers.xavier_initializer()
            binit = tf.constant_initializer(0)
            activ_fn = NNModel.get_activation_fn(self.config['activation'])
            normalizer_fn, normalizer_params = NNModel.get_normalizer(self.is_training,
                                                                      self.config['normalizer'])

            with slim.arg_scope([slim.fully_connected],
                                activation_fn=activ_fn,
                                weights_initializer=winit,
                                biases_initializer=binit,
                                normalizer_fn=normalizer_fn,
                                normalizer_params=normalizer_params):
                # build the bulk of the layers
                layers = slim.stack(self.x, slim.fully_connected,
                                    layer_sizes, scope="layer")

                # final layer has NO activation, NO BN
                return slim.fully_connected(layers, self.config['output_size'],
                                            activation_fn=None,
                                            normalizer_fn=None,
                                            weights_initializer=winit,
                                            biases_initializer=binit,
                                            scope='projection')

    def train(self, sess, loader, grapher, epoch, display_step=5):
        for data, labels in loader:
            feed_dict = {
                self.x: data,
                self.y: labels,
                self.is_training: True
            }

            # Display logs every kth epoch
            if epoch % display_step == 0:
                loss, summaries, _ = sess.run([self.loss, self.summaries, self.optim], feed_dict=feed_dict)
                print("[TRAIN Epoch:", '%04d]' % (epoch),
                      "loss = ", "{:.4f} | ".format(loss))
                grapher.add_summary(summaries, epoch)
            else:
                _ = sess.run([self.optim], feed_dict=feed_dict)

    def test_or_val(self, sess, loader, grapher, epoch):
        ''' iterate over all test data and provide mean stats '''
        loss_vector = []
        for data, labels in loader:
            feed_dict = {
                self.x: data,
                self.y: labels,
                self.is_training: False
            }
            # get all metrics here
            loss  = sess.run([self.loss], feed_dict=feed_dict)
            loss_vector.append(loss)

        # aggregate the summaries after we run over ALL the test data
        loss_mean = np.mean(loss_vector)
        print("[TEST/VAL Epoch:", '%04d]' % (epoch),
              "loss = ", "{:.4f} | ".format(loss_mean))
        loss_summary = tf.Summary()
        loss_summary.value.add(tag="test_loss", simple_value=loss_mean)
        grapher.add_summary(loss_summary, epoch)


def run(args):
    # get the dataloader
    loader = DataLoader(args)

    with tf.device(args.device): # CPU? GPU?
        gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=args.device_percentage)
        sess_cfg = tf.ConfigProto(allow_soft_placement=args.allow_soft_placement,
                                  gpu_options=gpu_options)
        with tf.Session(config=sess_cfg) as sess:
            model = NNModel(sess, args)    # build our model
            early = EarlyStopping(model)   # get the early stopper

            for epoch in range(args.epochs + 1):
                model.train(sess, loader.train, model.train_summary_writer, epoch)
                val_loss = model.test_or_val(sess, loader.val, model.val_summary_writer, epoch)

                # handle early stopping
                if early(val_loss):
                    early.restore(sess)
                    model.test_or_val(sess, loader.test, model.test_summary_writer, epoch)
                    break

                # evaluate test metrics
                model.test_or_val(sess, loader.test, model.test_summary_writer, epoch)


if __name__ == "__main__":
    # seed our RNGs
    np.random.seed(args.seed)
    tf.set_random_seed(args.seed)

    # build the dirs for storage of logs and models
    create_dir(args.output_dir)
    create_dir(os.path.join(args.output_dir, "logs"))
    create_dir(os.path.join(args.output_dir, "models"))

    # main entrypoint
    run(args)
