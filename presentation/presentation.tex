\documentclass[14pt]{beamer}

\usetheme{metropolis}
\usepackage{appendixnumberbeamer}

\usepackage{booktabs}
\usepackage[scale=2]{ccicons}

\usepackage{pgfplots}
\usepgfplotslibrary{dateplot}

\usepackage{xspace}
\newcommand{\themename}{\textbf{\textsc{metropolis}}\xspace}

% Fixes the spacing for beamer TOC %
\usepackage{etoolbox}
\makeatletter
\patchcmd{\beamer@sectionintoc}{\vskip1.5em}{\vskip0.5em}{}{}
\makeatother
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Custom packages
% \usepackage[none]{hyphenat} % Inserted this to not have those shitty -'s splitting lines
% \usepackage{amssymb}
% \usepackage{amsmath}
% \usepackage{enumitem}
% \usepackage{subcaption}
% \usepackage{gensymb}
% \usepackage{stmaryrd}
% \usepackage{fancyhdr}
% \usepackage{subcaption}
% \usepackage{stmaryrd}
\usepackage{bm}
% \usepackage{relsize}
% \usepackage{graphicx} % more modern
% \graphicspath{ {imgs/} }
% \usepackage{natbib}% For citations
% \newtheorem{theorem}{Theorem}[section]
% \newtheorem{corollary}{Corollary}[theorem]


\title{Tensorflow vs. Pytorch \& General ML Coding Guidelines}
% \subtitle{Extending generative models }
\date{\today}
\author{Jason Ramapuram}
\institute{University of Geneva \& \\ University of Applied Sciences Western
  Switzerland}
% \titlegraphic{\hfill\includegraphics[height=1.5cm]{logo.pdf}}

\begin{document}

\maketitle

\begin{frame}{Table of contents}
  \setbeamertemplate{section in toc}[sections numbered]
  \tableofcontents[hideallsubsections]
\end{frame}

\section{Introduction}
\begin{frame}[fragile]{Machine Learning Requires:}
  \begin{center}
    \includegraphics[width=90mm]{imgs/Data_Science_VD}
  \end{center}
\end{frame}

\begin{frame}[fragile]{Striving for simplicity}
  \begin{center}
  \emph{"when you have two competing theories that make exactly the same
    predictions, the simpler one is the better."} \footnote{Some of the
    items to follow are opinion based and if a better solution exists please let me know!}
  \end{center}
\end{frame}


\section{General ML Related}
\begin{frame}[fragile]{DO parameterize your hyper-parameters}
  \begin{center}
    \includegraphics[width=115mm]{imgs/hyperparam}
  \end{center}
\end{frame}

\begin{frame}[fragile]{DO Uniquely Parameterize Experiments}
  \begin{center}
    \includegraphics[width=100mm]{imgs/uid}
  \end{center}
\end{frame}

\begin{frame}[fragile]{DO use tensorboard or visdom}
  \begin{center}
    \includegraphics[height=80mm]{imgs/tensorboard_visdom}
  \end{center}
\end{frame}

\begin{frame}[fragile]{DO (try) to use Sacred}
  \begin{center}
    \includegraphics[height=75mm]{imgs/sacred}
  \end{center}
\end{frame}

\begin{frame}[fragile]{DO structure code logically}
  \begin{center}
    \includegraphics[height=60mm]{imgs/structure}
  \end{center}
  \emph{DONT} go overboard on abstractions
\end{frame}

\begin{frame}[fragile]{DO keep a disparate datasets \& utils repo}
  \begin{center}
    \includegraphics[height=60mm]{imgs/datasets}
  \end{center}
  Keep your data-loaders / datasets and utils here
\end{frame}

\begin{frame}[fragile]{Datasets related}
  \begin{center}
    \begin{itemize}
    \item \emph{DO} use iterators: \includegraphics[height=5mm]{imgs/iterator}
    \item \emph{DO} use standard readers if available
    \item \emph{DO} store your datasets in a tfrecord (tensorflow) or simple
      generator (pytorch)
    \end{itemize}
  \end{center}
\end{frame}


\begin{frame}[fragile]{Git related}
  \begin{center}
    \begin{itemize}
      \item only commit working code to master
      \item multiple people can work together via feature branches
        (feature/hessian\_loss) \footnote{\url{https://danielkummer.github.io/git-flow-cheatsheet/}}
      \item \emph{NEVER} do \emph{git add -A}, \emph{ALWAYS} check with \emph{git status} and \emph{git diff} and manually add
      \item Use lfs for large files
      \end{itemize}
  \end{center}
\end{frame}

\begin{frame}[fragile]{Suggestion: SSHFS for prototyping}
  \begin{center}
    \begin{itemize}
    \item SSHFS is a \emph{PULL} based file-system that sends data over ssh
      [encryption guarantees]
    \item Better than dropbox (due to sync delay)
    \item Allows you to work locally on machine and have latest code
      \emph{PULLED} when executing remotely
    \item Probably not a good idea to SSHFS 100 parallel runs (Network bottleneck)
    \end{itemize}
  \end{center}
\end{frame}

\begin{frame}[fragile]{iPython for prototyping/demos only}
  iPython is great for early prototyping / demos, but adds overhead and doesn't allow for good
  abstraction. Try to use structure presented above (or similar logical
  structure) when building projects (especially when deploying to servers).
\end{frame}


\section{General Python Guidelines}

\begin{frame}[fragile]{DO Use ArgParse}
  \begin{center}
    \includegraphics[width=105mm]{imgs/argparse}
  \end{center}
\end{frame}

\begin{frame}[fragile]{DONT Use sys.args}
  \begin{center}
    \includegraphics[width=90mm]{imgs/noargparse}
  \end{center}
\end{frame}

\begin{frame}[fragile]{DO use a virtual environment}
  \begin{center}
    \begin{itemize}
      \item Anaconda [ recommended ]: fully self-contained; can install GCC /
        opencv, etc
      \item Virtualenv: virtual python environment ONLY (you will have to
        compile things like opencv)
      \end{itemize}
  \end{center}
\end{frame}

\begin{frame}[fragile]{DO Use Shebang Line}
  \begin{center}
    \includegraphics[width=90mm]{imgs/shebang}
  \end{center}
\end{frame}

\begin{frame}[fragile]{DONT Use SPECIFIC Shebang Line}
  \begin{center}
    \includegraphics[width=90mm]{imgs/noshebang}
  \end{center}
\end{frame}

\begin{frame}[fragile]{DO check for \_\_main\_\_}
  \begin{center}
    \includegraphics[width=90mm]{imgs/main}
  \end{center}
\end{frame}

\begin{frame}[fragile]{DO follow standards}
  In general:
  \begin{itemize}
    \item {\small \url{google.github.io/styleguide/pyguide.html}}
    \item {\small \url{python.org/dev/peps/pep-0008/}}
  \end{itemize}
\end{frame}

\section{Tensorflow Related}
\begin{frame}[fragile]{Understanding Summaries}
  \begin{center}
    \includegraphics[height=40mm]{imgs/tf_summaries}
    \vskip -0.3cm
    \begin{itemize}
    \item Summaries include \emph{a writer} and \emph{individual or aggregated
        summaries}
    \item You can use histogram, distribution, image, video, audio, scalar (and
      more) summaries in tf
    \end{itemize}
  \end{center}
\end{frame}

\begin{frame}[fragile]{Summaries Best Practices}
  \begin{center}
    \begin{itemize}
    \item Use a different writer for \emph{test}, \emph{train} and \emph{validation}
    \item Always writing summaries will massively slow down your model (IO bound)
    \item Write image summaries \emph{FAR} less often (eg: once every 5-10 epochs)
    \item Remember tensorboard \emph{ONLY} let's you download 1000 points by default
    \end{itemize}
  \end{center}
\end{frame}

\begin{frame}[fragile]{Use a simple train-loop}
  \begin{center}
    \includegraphics[width=115mm]{imgs/train_loop}
  \end{center}
\end{frame}

\begin{frame}[fragile]{Use a simple test/val loop (and combine if possible)}
  \begin{center}
    \includegraphics[width=100mm]{imgs/test_loop}
  \end{center}
\end{frame}

\begin{frame}[fragile]{Has Estimator API (similar to sklearn)}
  \begin{center}
    \includegraphics[width=70mm]{imgs/estimator}
  \end{center}
\end{frame}

\section{Pytorch Related}

\begin{frame}[fragile]{What is a dynamic graph?\footnote{yes I know about dynamic\_rnn}}
  \begin{center}
    \includegraphics[height=30mm]{imgs/dynamic_graph}
  \end{center}
\end{frame}

\begin{frame}[fragile]{What is a dynamic graph?\footnote{yes I know about
      tf.cond, but that is not a dynamic operation, more like a c++ template; tf
      eager is promising though}}
  \begin{center}
    \includegraphics[height=30mm]{imgs/dynamic_graph2}
  \end{center}
\end{frame}

\begin{frame}[fragile]{torchvision: two line models}
  \begin{center}
    \includegraphics[height=13mm]{imgs/torchvision}
  \end{center}
\end{frame}

\begin{frame}[fragile]{Nicely Abstracted DataLoader vs. Dataset}
  \begin{center}
    \includegraphics[height=70mm]{imgs/dataset}
  \end{center}
\end{frame}

\begin{frame}[fragile]{Nicely Abstracted DataLoader vs. Dataset}
  \begin{center}
    \includegraphics[height=70mm]{imgs/loader}
  \end{center}
\end{frame}

\begin{frame}[fragile]{Pytorch: still in early-beta}
  \begin{itemize}
  \item No proper support for distributions (tf.distributions) **yet**
  \item Nice modular abstractions with nn containers
  \item nn.functional is great for on-the-fly ops
  \item Lots of goodies baked in (eg: layers); in tf should we use slim? keras? prettytensor?
  \item \emph{SUPER} easy multi-GPU batch parallelization
  \item Much easier to read code
  \end{itemize}
\end{frame}


\section{Pytorch vs. Tensorflow}

\begin{frame}[fragile]{Tensorflow: Copy Layer At Runtime}
  \begin{center}
    \includegraphics[height=40mm]{imgs/tf_copy}
  \end{center}
\end{frame}

\begin{frame}[fragile]{Pytorch: Copy Layer At Runtime}
  \begin{center}
    \includegraphics[height=30mm]{imgs/pytorch_copy}
  \end{center}
\end{frame}

\begin{frame}[fragile]{Tensorflow: PITA\footnote{but allows for fine grained control} device management}
  \begin{center}
    \includegraphics[height=40mm]{imgs/tf_mgpu}
  \end{center}
\end{frame}

\begin{frame}[fragile]{Pytorch: Effortless batch parallelization}
  \begin{center}
    \includegraphics[height=10mm]{imgs/pytorch_mgpu}
  \end{center}
\end{frame}

\begin{frame}[fragile]{Tensorflow: Sample Model}
  \begin{center}
    \includegraphics[height=32mm]{imgs/tf_model}
    And then \emph{sess.run(tf.global\_variable\_initializer())}...
  \end{center}
\end{frame}

\begin{frame}[fragile]{Pytorch: Sample Model}
  \begin{center}
    \includegraphics[height=40mm]{imgs/pytorch_model}
  \end{center}
\end{frame}

\section{Conclusion}

\begin{frame}{Summary}

  Source code for this presentation is available at:

  \begin{center}\url{bitbucket.org/dmmlgeneva/code_day_2018}\end{center}

\end{frame}

\begin{frame}[standout]
  Questions?
\end{frame}

\appendix

% \begin{frame}[fragile]{Backup slides}
%   Sometimes, it is useful to add slides at the end of your presentation to
%   refer to during audience questions.

%   The best way to do this is to include the \verb|appendixnumberbeamer|
%   package in your preamble and call \verb|\appendix| before your backup slides.

%   \themename will automatically turn off slide numbering and progress bars for
%   slides in the appendix.
% \end{frame}

% \begin{frame}[allowframebreaks]{References}

%   \bibliography{presentation}
%   \bibliographystyle{abbrv}

% \end{frame}

\end{document}
